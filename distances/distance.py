import numpy as np
import numpy.typing as npt


def _distance_matrix(x_points: npt.ArrayLike, y_points: npt.ArrayLike):
    return np.sqrt(
        ((x_points[:, np.newaxis, :] - y_points[np.newaxis, :, :]) ** 2).sum(axis=-1)
    )


def minimal(x_points: npt.ArrayLike, y_points: npt.ArrayLike):
    return _distance_matrix(x_points, y_points).min()


def hausdorff(x_points: npt.ArrayLike, y_points: npt.ArrayLike):
    dists = _distance_matrix(x_points, y_points)
    return max(dists.min(axis=0).max(), dists.min(axis=1).max())


if __name__ == "__main__":
    X = np.loadtxt("x.txt")
    Y = np.loadtxt("y.txt")

    dist_min = minimal(X, Y)
    print(f"Distance min : {dist_min}")

    dist_hd = hausdorff(X, Y)
    print(f"Distance Hausdorff : {dist_hd}")
